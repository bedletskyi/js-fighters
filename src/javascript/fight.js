export function fight(firstFighter, secondFighter) {
  // return winner
  let { health: firstFighterHealth } = firstFighter;
  let { health: secondFighterHealth } = secondFighter;

  while (true) {
    if (firstFighterHealth <= 0) {
      return secondFighter;
    } else if (secondFighterHealth <= 0) {
      return firstFighter;
    } else {
      secondFighterHealth -= getDamage(firstFighter, secondFighter);
      firstFighterHealth -= getDamage(secondFighter, firstFighter);
    }
  }
}

export function getDamage(attacker, enemy) {
  // damage = hit - block
  // return damage 
  const hit = getHitPower(attacker);
  const block = getBlockPower(enemy);
  const damage = (hit - block) < 0 ? 0 : hit - block;

  return damage;
}

export function getHitPower(fighter) {
  // return hit power
  const { attack } = fighter;
  const criticalHitChance = Math.random() + 1;
  const power = attack * criticalHitChance;

  return power;
}

export function getBlockPower(fighter) {
  // return block power
  const { defense } = fighter;
  const dodgeChance = Math.random() + 1;
  const blockPower = defense * dodgeChance;

  return blockPower;
}
