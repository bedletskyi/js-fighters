import { createElement } from '../helpers/domHelper';
import { showModal } from './modal';

export  function showFighterDetailsModal(fighter) {
  const title = 'Fighter info';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter) {
  const { name, health, attack, defense, source } = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const imgElement = createElement({ tagName: 'img', className: 'fighter-image', attributes: {
    src: source,
    style: 'display: block; margin: auto;'
  }});
  const nameElement = createElement({ tagName: 'p', className: 'name', attributes: {style: 'text-align: center;'}});
  const healthElement = createElement({ tagName: 'p', className: 'fighter-health' });
  const attackElement = createElement({ tagName: 'p', className: 'fighter-attack' });
  const defenceElement = createElement({ tagName: 'p', className: 'fighter-defence' });
  
  // show fighter name, attack, defense, health, image

  nameElement.innerText = name;
  healthElement.innerText = `Health: ${health}`;
  attackElement.innerText = `Attack: ${attack}`;
  defenceElement.innerText = `Defense: ${defense}`;
  fighterDetails.append(imgElement, nameElement, healthElement, attackElement, defenceElement);

  return fighterDetails;
}
