import { createElement } from "../helpers/domHelper";
import { showModal } from './modal';

export  function showWinnerModal(fighter) {
  const title = 'WINNER';
  const bodyElement = createFighterDetails(fighter);
  showModal({ title, bodyElement });
}

function createFighterDetails(fighter) {
  const { name, source } = fighter;

  const fighterDetails = createElement({ tagName: 'div', className: 'modal-body' });
  const imgElement = createElement({ tagName: 'img', className: 'fighter-image', attributes: {
    src: source,
    style: 'display: block; margin: auto;'
  }});
  const nameElement = createElement({ tagName: 'p', className: 'name', attributes: {style: 'text-align: center;'}});

  nameElement.innerText = name;

  fighterDetails.append(imgElement, nameElement);

  return fighterDetails;
}